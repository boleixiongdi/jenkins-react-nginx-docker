/**
 * uat 环境配置
 */
import { defineConfig } from 'umi';

export default defineConfig({
  define: {
    // 全局常量
    'process.env.routerMode': 'hash',
    'process.env.state': '1',

    // ngfe-request 所需全局常量
    NGFE_REQUEST: {
      // 开发环境是否开启 mock
      mock: false,
     // 请求基础 URL 地址，询问 后端 获取
     baseUrl: 'http://192.168.17.80:30096',
     // 设置需配置在 http header 中 aacCode（能力中心），询问项目经理/后端获取
     headerAacCode: '777',
     // 设置需配置在 http header 中 cadCode（能力域），询问项目经理/后端获取
     headerCadCode: '100777',
       // 设置渠道号
     chnlNo: '10095',
    },
  },
});
