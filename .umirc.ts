import { defineConfig } from 'umi';
import routes from './config/routes';

export default defineConfig({
  // layout: {
  //   name: '开发环境临时菜单',
  //   // 是否开启国际化配置
  //   locale: false,
  // },
  nodeModulesTransform: {
    type: 'none',
  },
  // 浏览器兼容
  targets: {
    ie: 11,
  },
  // 开启配置qiankun
  qiankun: {
    slave: {},
  },
  // 路由模式
  history: { type: 'hash' },
  // 开启dva
  dva: {
    // 开启dva-immer，用于代理currentState和nextState之间的改变，即当前状态修改副本
    immer: true,
    // 开启模块热加载(热更新)
    hmr: true,
  },
  // 开启antd
  antd: {},
  // 路由  可单独抽离  这里的路由配置默认走layouts ，可根据项目组需求选择删除
  routes,
});
