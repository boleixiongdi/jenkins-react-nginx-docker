// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '中文名称',
    dataIndex: 'chineseName',
    key: 'chineseName',
  },
  {
    title: '英文全称',
    dataIndex: 'englishName',
    key: 'englishName',
  },
  {
    title: '英文缩写',
    dataIndex: 'englishAbbreviation',
    key: 'englishAbbreviation',
  },
  {
    title: '备注',
    dataIndex: 'remark',
    key: 'remark',
  },
  {
    title: '修订人',
    dataIndex: 'updTlr',
    key: 'updTlr',
  },
  {
    title: '修订时间',
    dataIndex: 'updDte',
    key: 'updDte',
  },

 
];
export default columnsData;
