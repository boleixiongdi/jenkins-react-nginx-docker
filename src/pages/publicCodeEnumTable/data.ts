// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '代码名称',
    dataIndex: 'codeName',
    key: 'codeName',
  },
  {
    title: '枚举名称',
    dataIndex: 'enumName',
    key: 'enumName',
  },
  {
    title: '业务定义',
    dataIndex: 'definition',
    key: 'definition',
  },
  {
    title: '中文名称',
    dataIndex: 'chineseName',
    key: 'chineseName',
  },
  {
    title: '备注',
    dataIndex: 'remark',
    key: 'remark',
  }
];
export default columnsData;
