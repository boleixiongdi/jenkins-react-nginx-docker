/* 用户信息管理页 类组件编程*/
import { Space, Form, Button, Select } from 'antd';
import { FormInstance } from 'antd/es/form';
import ExportJsonExcel from 'js-export-excel';

const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: 'left'
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};


import React, { Component } from 'react';
import {
  FcTable,
  FcButton,
  FcInput,
  FcMessage,
  FcModal,
  FcDivider,
  FcCard,
} from '@ngfed/fc-components';
import {
  getConditionSearch,
  getUserList,
  userDelete,
  userListAdd,
  userListUpdate,
} from './service';
import columnsData from './data';
import styles from './index.less';

class DataTable extends Component {
  formRef = React.createRef<FormInstance>();
  editformRef = React.createRef<FormInstance>();
  searchformRef = React.createRef<FormInstance>();


  state = {
    //转跳携带参数
    codeName:'',
     // 根据条件查询的列表数据
    datas: [],
    // 表头
    columns: [],
    // 删除提示框显示状态
    isModalVisible: false,
    //下载提示框显示状态
    isDownloadVisible: false,
    //下载链接
    downloadLink: '',
    // 删除信息Id和修改信息Id
    id: '',
    // 查询条件姓名
    name: '',
    // 用户添加模态框显示状态
    addModal: false,
    // 用户编辑模态框显示状态
    editModal: false,
    // 当前页码
    current: 1,
    // 每页显示数量
    pageSize: 5,

    selectedRowKeys: [], // Check here to configure the default column

    loading: false,

    huixian:[]


  };

  
  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  onFinish = (values: any) => {
    console.log(values);
    const codename=this.state.codeName
    userListAdd(values,codename).then(async (res: any) => {
      this.setState({ addModal: false });
      FcMessage.success(res.sysHead.retInf);
      (this as any).formRef.current.resetFields();
      this.getDatas(this.state.codeName);

    });
  };
  onFinishEdit = (values: any) => {
    console.log("sssaaadddssx" + this.state.id)
    console.log(values);
    userListUpdate(this.state.id, values).then(async (res: any) => {
      if (res.sysHead.retCd === '000000') {
        this.getDatas(this.state.codeName);
        this.setState({ editModal: false });
        FcMessage.success(res.sysHead.retInf);

      }
    });
  };

  componentWillMount(){
    this.setState({
        codeName:this.props.location.state.codeName
    })
}

  // 生命周期 数据初始化方法建议写在这个里面
  componentDidMount() {

    this.tableHeader();
    //不是错误
    this.getDatas(this.state.codeName);


  }

  // 表格头部数据重组
  tableHeader = () => {
    let columns: any = columnsData;
    const obj = {
      title: 'address',
      dataIndex: 'address',
      key: 'address',
      render: (text: any, record: any) => (
        <div>
          <a onClick={() => this.handleRemove(record.seqNo)}>删除</a>
        </div>
      ),
    };

    columns = [...columns, obj];
    this.setState({
      columns,
    });
  };

  /**
   * 数据查询
   * @param params 查询的参数对象
   */
  getDatas = (params:string) => {
    getUserList(params).then(async (res: any) => {
      console.log('res', res?.sysHead?.retCd);
      this.setState({
        datas: res.body,
      });
    });
  };

  //条件搜索
  getConditionSearch = (params: object) => {
    console.log(params)
    getConditionSearch(params).then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };


  /**
   * 显示模态框提示是否删除
   * @param id 用户id
   */
  handleRemove = (id: string) => {
    this.setState({
      id,
      isModalVisible: true,
    });
  };

  // 确定删除用户数据
  handleOk = () => {
    userDelete({ seqNo: this.state.id }).then((res: any) => {

      if (res.sysHead.retCd === '000000') {
        this.getDatas(this.state.codeName);
        this.setState({ isModalVisible: false });
        FcMessage.success(res.sysHead.retInf);
      }
    });
  };






  onReset = () => {
    this.searchformRef.current!.resetFields();
    this.getDatas(this.state.codeName)
  };

  




  render() {

    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        {/* className 规范 */}
        {/* 单个class */}
        {/* <div  className={ styles.boxTitle }></div> */}
        {/* 多个class */}
        {/* <div  className={ `${styles.boxTitle} ${styles.boxText}` }></div> */}
        <div className={styles.box}>
          <Form {...layout} ref={this.searchformRef} name="control-ref" onFinish={this.getConditionSearch} labelAlign={'left'} layout="inline">
            <Form.Item label="枚举名称" name="enumName" >
              <FcInput style={{ width: 100 }}
              />
            </Form.Item>
            <Form.Item label="业务定义" name="definition" >
              <FcInput />
            </Form.Item>
            <Form.Item label="中文名称" name="chineseName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="备注" name="remark" >
              <FcInput />
            </Form.Item>
            <Form.Item style={{ marginRight: 8 }}
            >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </Form.Item>

            <Button htmlType="button" onClick={this.onReset}>
              Reset
            </Button>
          </Form>

        </div>

        <FcCard
          title="用户信息"
          extra={
            <FcButton
              type="primary"
              onClick={() => {
                this.setState({ addModal: true });
              }}
            >
              {' '}
              +添加
            </FcButton>


          }
          bordered={false}
        >

          <div>
            <div style={{ marginBottom: 16 }}>

      
            </div>
            <FcTable rowKey="seqNo" rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.datas} />
          </div>
        </FcCard>

        <FcModal
          cancelText="取消"
          okText="确认"
          title="提示"
          visible={this.state.isModalVisible}
          onOk={this.handleOk}
          onCancel={() => {
            this.setState({ isModalVisible: false });
          }}
        >
          是否确定删除本条数据？
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="新增"
          title="用户新增"
          visible={this.state.addModal}
          onOk={this.onFinish}
          onCancel={() => {
            this.setState({
              addModal: false,
            });
          }}
        >
          <Form {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish} labelAlign={'left'}>
          <Form.Item label="枚举名称" name="enumName" >
              <FcInput style={{ width: 100 }}
              />
            </Form.Item>
            <Form.Item label="业务定义" name="definition" >
              <FcInput />
            </Form.Item>
            <Form.Item label="中文名称" name="chineseName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="备注" name="remark" >
              <FcInput />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>

      

     
      </div>
    );
  }
}

export default DataTable;
