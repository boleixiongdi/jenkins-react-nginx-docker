import { request } from 'ngfe-request';
// 目前我这里的接口用是模客数据

// 用户查询
export function getUserList(params: string) {
  
  // 此处的 /function/info/list 是提供给 mock 使用的，在生产环境中无效
  return request('/api/dy/publicCodeEnumSelect', {
    // HTTP 请求体
    data: {
      // 系统报文头
      sysHead: {
        stdSvcInd: 'PublicCodeEnumSVC',
        stdIntfcInd: 'queryByCodeName',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      // 本地报文头
      localHead: {
        "reptChkSrlNo": "",
        "smyCdDsc": "",
        "crnPgCnt": "",
        "totLineNum1": ""
      },
      // 报文体
      body: {
        codeName:params
      },
    },
  });
}
//条件搜索
export function getConditionSearch(params: object) {
  
  // 此处的 /function/info/list 是提供给 mock 使用的，在生产环境中无效
  return request('/api/dy/publicCodeEnumQueryAllByLimit', {
    // HTTP 请求体
    data: {
      // 系统报文头
      sysHead: {
        stdSvcInd: 'PublicCodeEnumSVC',
        stdIntfcInd: 'queryAllByLimit',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      // 本地报文头
      localHead: {
        "reptChkSrlNo": "",
        "smyCdDsc": "",
        "crnPgCnt": "",
        "totLineNum1": ""
      },
      // 报文体
      body: {
        ...params
      },
    },
  });
}

// 用户数据删除
export async function userDelete(params: object) {
  console.log("delete:")
  console.log(params)
  return request('/api/user/publicCodeEnumDeleteById', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'PublicCodeEnumSVC',
        stdIntfcInd: 'deleteById',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,
      },
    },
  });
}

// 用户数据添加
export async function userListAdd(params: object,codeName ) {
  return request('/api/user/publicCodeEnumInsert', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'PublicCodeEnumSVC',
        stdIntfcInd: 'insert',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,codeName:codeName
      },
    },
  });
}

// 用户数据修改
export async function userListUpdate(id: string,params: object) {
  return request('/api/user/publicCodeEnumUpdate', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'PublicCodeEnumSVC',
        stdIntfcInd: 'update',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,seqNo:id
      },
    },
  });
}

