// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '标准类型',
    dataIndex: 'type',
    key: 'type',
  },
  {
    title: '中文名称',
    dataIndex: 'chineseName',
    key: 'chineseName',
  },
  {
    title: '中文别名',
    dataIndex: 'chineseNameAlias',
    key: 'chineseNameAlias',
  },
  {
    title: '英文名称',
    dataIndex: 'englishName',
    key: 'englishName',
  },
  {
    title: '英文简称',
    dataIndex: 'englishNameShort',
    key: 'englishNameShort',
  },
  {
    title: '业务含义',
    dataIndex: 'meaning',
    key: 'meaning',
  },
  {
    title: '数据格式',
    dataIndex: 'dataFormate',
    key: 'dataFormate',
  },
  {
    title: '取值范围',
    dataIndex: 'scope',
    key: 'scope',
  },
  {
    title: '代码编码规则',
    dataIndex: 'codingRule',
    key: 'codingRule',
  },
  {
    title: '制定依据',
    dataIndex: 'reason',
    key: 'reason',
  }
 
];
export default columnsData;
