import { request } from 'ngfe-request';
// 目前我这里的接口用是模客数据

// 用户查询
export function getUserList() {
  
  // 此处的 /function/info/list 是提供给 mock 使用的，在生产环境中无效
  return request('/api/dy/retrievalSelect', {
    // HTTP 请求体
    data: {
      // 系统报文头
      sysHead: {
        stdSvcInd: 'RetrievalSVC',
        stdIntfcInd: 'select',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      // 本地报文头
      localHead: {
        "reptChkSrlNo": "",
        "smyCdDsc": "",
        "crnPgCnt": "",
        "totLineNum1": ""
      },
      // 报文体
      body: {
      },
    },
  });
}
//条件搜索
export function getConditionSearch(params: object) {
  
  // 此处的 /function/info/list 是提供给 mock 使用的，在生产环境中无效
  return request('/api/dy/retrievalQueryAllByLimit', {
    // HTTP 请求体
    data: {
      // 系统报文头
      sysHead: {
        stdSvcInd: 'RetrievalSVC',
        stdIntfcInd: 'queryAllByLimit',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      // 本地报文头
      localHead: {
        "reptChkSrlNo": "",
        "smyCdDsc": "",
        "crnPgCnt": "",
        "totLineNum1": ""
      },
      // 报文体
      body: {
        ...params
      },
    },
  });
}

// 用户数据删除
export async function userDelete(params: object) {
  console.log("delete:")
  console.log(params)
  return request('/api/user/retrievalDeleteById', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'RetrievalSVC',
        stdIntfcInd: 'deleteById',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,
      },
    },
  });
}

// 用户数据添加
export async function userListAdd(params: object) {
  return request('/api/user/retrievalInsert', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'RetrievalSVC',
        stdIntfcInd: 'insert',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,
      },
    },
  });
}

// 用户数据修改
export async function userListUpdate(id: string,params: object) {
  return request('/api/user/retrievalUpdate', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'RetrievalSVC',
        stdIntfcInd: 'update',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,seqNo:id
      },
    },
  });
}



// 导出pdf
export async function userListExportPdf(params: object) {
  return request('/api/user/retrievalPDFDownload', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'RetrievalSVC',
        stdIntfcInd: 'retrievalPDFDownload',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: { 
        retrievalReqsList:params  
        
      },
    },
  });
}

