// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '接口ID',
    dataIndex: 'interfaceId',
    key: 'interfaceId',
  },
  {
    title: '服务代码',
    dataIndex: 'svcCode',
    key: 'svcCode',
  },
  {
    title: '服务标识',
    dataIndex: 'svcInd',
    key: 'svcInd',
  },
  {
    title: '服务名称',
    dataIndex: 'svcIndName',
    key: 'svcIndName',
  },
  {
    title: '标准接口标识',
    dataIndex: 'stdIntfInd',
    key: 'stdIntfInd',
  },
  {
    title: '标准接口名称',
    dataIndex: 'stdIntfIndName',
    key: 'stdIntfIndName',
  },
  {
    title: '消费方',
    dataIndex: 'consumer',
    key: 'consumer',
  },
  {
    title: '消费方服务标识',
    dataIndex: 'consumerSvcInd',
    key: 'consumerSvcInd',
  },
  {
    title: '消费方服务名称',
    dataIndex: 'consumerSvcIndName',
    key: 'consumerSvcIndName',
  },
  {
    title: '提供方',
    dataIndex: 'prodiver',
    key: 'prodiver',
  },
  {
    title: '提供方服务标识',
    dataIndex: 'prodiverSvcInd',
    key: 'prodiverSvcInd',
  },
  {
    title: '提供方服务名称',
    dataIndex: 'prodiverSvcIndName',
    key: 'prodiverSvcIndName',
  },
  {
    title: '版本号',
    dataIndex: 'version',
    key: 'version',
  },
  {
    title: '接口状态',
    dataIndex: 'interfaceStuat',
    key: 'interfaceStuat',
  },
  {
    title: '服务BSD版本',
    dataIndex: 'svcBsdVersion',
    key: 'svcBsdVersion',
  },
  {
    title: '场景功能描述',
    dataIndex: 'sceneDescription',
    key: 'sceneDescription',
  }
 
];
export default columnsData;
