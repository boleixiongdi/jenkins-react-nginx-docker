/* 用户信息管理页 类组件编程*/
import { Space, Form, Button, Select } from 'antd';
import { FormInstance } from 'antd/es/form';
import ExportJsonExcel from 'js-export-excel';
import { router } from 'dva';
const { Link } = router;
const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: 'left'
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};


import React, { Component } from 'react';
import {
  FcTable,
  FcButton,
  FcInput,
  FcMessage,
  FcModal,
  FcDivider,
  FcCard,
} from '@ngfed/fc-components';
import {
  getUserList,
  userDelete,
  userListAdd,
  userListUpdate,
  getConditionSearch,
  userListExportPdf
} from './service';
import columnsData from './data';
import styles from './index.less';

class DataTable extends Component {
  formRef = React.createRef<FormInstance>();
  editformRef = React.createRef<FormInstance>();
  searchformRef = React.createRef<FormInstance>();


  state = {
    // 根据条件查询的列表数据
    datas: [],
    // 表头
    columns: [],
    // 删除提示框显示状态
    isModalVisible: false,
    // 删除信息Id和修改信息Id
    id: '',
    // 查询条件姓名
    name: '',
    // 用户添加模态框显示状态
    addModal: false,
    // 用户编辑模态框显示状态
    editModal: false,
    // 当前页码
    current: 1,
    // 每页显示数量
    pageSize: 5,

    selectedRowKeys: [], // Check here to configure the default column

    loading: false,

    isDownloadXmlVisible: false,
    xmlLink: '',
    editProperty: []
  };

 
  //导出execl方法
  exportExcel = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
      });
    }, 1000);
    let arr = new Set(this.state.datas)
    let arr2 = new Set(this.state.selectedRowKeys)
    let arr3 = Array.from(arr.filter(value => {
      return arr2.has(value.seqNo)
    }))

    this.handleExportCurrentExcel(arr3)
  }


  //表单的多选
  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  //添加的接口
  onFinish = (values: any) => {
    console.log(values);
    userListAdd(values).then(async (res: any) => {
      this.setState({ addModal: false });
      FcMessage.success(res.sysHead.retInf);
      (this as any).formRef.current.resetFields();
      this.getDatas()
    });


  };
  //修改的接口
  onFinishEdit = (values: any) => {
    console.log(values);
    userListUpdate(this.state.id, values).then(async (res: any) => {
      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ editModal: false });
        FcMessage.success(res.sysHead.retInf);
    
      }
    });
  };





  // 生命周期 数据初始化方法建议写在这个里面
  componentDidMount() {
    
    this.tableHeader();
    
    this.getDatas();
  }

  // 表格头部数据重组
  tableHeader = () => {
    let columns: any = columnsData;
    const obj = {
      title: '操作',
      dataIndex: 'address',
      key: 'address',
      render: (text: any, record: any) => (
        <div>
          <a onClick={() => this.editUser(record)}>修改</a>
          <FcDivider type="vertical" />
          <a onClick={() => this.handleRemove(record.seqNo)}>删除</a>
          <FcDivider type="vertical" />
          <Link to={
            {
              pathname: `/call`,
              state: { retrievalVO: record }
            }
          }>
            服务场景</Link>
          <FcDivider type="vertical" />
          <Link to={
            {
              pathname: `/sla`,
              state: { retrievalVO: record }
            }
          }>
            服务SLA</Link>
        </div>
      ),
    };

    columns = [...columns, obj];
    this.setState({
      columns,
    });
  };

  /**
   * 数据查询
   * @param params 查询的参数对象
   */
  getDatas = () => {
    getUserList().then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };
  /**
   * 数据条件查询
   * @param params 查询的参数对象
   */
  ConditionSearch = (params: object) => {
    getConditionSearch(params).then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };


  /**
   * 显示模态框提示是否删除
   * @param id 用户id
   */
  handleRemove = (id: string) => {
    this.setState({
      id,
      isModalVisible: true,
    });
  };

  // 确定删除用户数据
  handleOk = () => {
    userDelete({ seqNo: this.state.id }).then((res: any) => {

      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ isModalVisible: false });
        FcMessage.success(res.sysHead.retInf);
      }
    });
  };



  /**
   * 编辑-点击打开模态框-回显
   * @param item 行对象数据
   */
  editUser = async (item: any) => {
    await this.setState({
      editModal: true,
      id: item.seqNo,
      editProperty: item
    });
    // setTimeout(() => {
    //   (this as any).editFormRef.current.setFieldsValue(item);
    // }, 0);
  };

  // 保存编辑值-修改
  editUserOK = () => {
    const param = (this as any).editFormRef.current.getFieldsValue();
    console.log("修改++++++" + this.state.id)
  };

  //重置查询输入框
  onReset = () => {
    this.searchformRef.current!.resetFields();
  };

  // 导出EXECL
  handleExportCurrentExcel = (ExcelData: any) => { //ExcelData为后端返回的data数组
    let sheetFilter = ["seqNo", "interfaceId", "svcCode", "svcInd", "svcIndName", "stdIntfInd", "stdIntfIndName", 'consumer', "consumerSvcInd", "consumerSvcIndName", "prodiver", "prodiverSvcInd", "prodiverSvcIndName", "version", "interfaceStuat", "svcBsdVersion", "sceneDescription"]
    let option = {};
    option.fileName = '原子服务接口表';
    option.datas = [
      {
        sheetData: ExcelData,
        sheetName: '原子服务接口表',
        sheetFilter: sheetFilter,
        sheetHeader: ['顺序号', '接口ID', '服务代码', '服务标识', '服务名称', '标准接口标识', '标准接口名称', '消费方', '消费方服务标识', '消费方服务名称', '提供方', '提供方服务标识', '提供方服务名称', '版本号', '接口状态', '服务BSD版本', '场景功能描述'],
        columnWidths: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
      },
    ];

    var toExcel = new ExportJsonExcel(option); //new
    toExcel.saveExcel(); //保存
  }




  render() {
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        {/* className 规范 */}
        {/* 单个class */}
        {/* <div  className={ styles.boxTitle }></div> */}
        {/* 多个class */}
        {/* <div  className={ `${styles.boxTitle} ${styles.boxText}` }></div> */}
        <div className={styles.box}>
          <Form {...layout} ref={this.searchformRef} name="control-ref" onFinish={this.ConditionSearch} labelAlign={'left'} layout="inline">
            <Space size={[8, 16]} wrap>
              <Form.Item label="接口ID" name="interfaceId" >
                <FcInput />
              </Form.Item>
              <Form.Item label="服务代码" name="svcCode" >
                <FcInput />
              </Form.Item>
              <Form.Item label="服务标识" name="svcInd" >
                <FcInput />
              </Form.Item>
              <Form.Item label="服务名称" name="svcIndName" >
                <FcInput />
              </Form.Item>
              <Form.Item label="标准接口标识" name="stdIntfInd" >
                <FcInput />
              </Form.Item>
              <Form.Item label="标准接口名称" name="stdIntfIndName" >
                <FcInput />
              </Form.Item>
              <Form.Item label="消费方" name="consumer" >
                <FcInput />
              </Form.Item>
              <Form.Item label="提供方" name="prodiver" >
                <FcInput />
              </Form.Item>
              <Form.Item label="接口状态" name="interfaceStuat" >
                <FcInput />
              </Form.Item>
              <Form.Item label="服务BSD版本" name="svcBsdVersion" >
                <FcInput />
              </Form.Item>
              <Form.Item label="版本号" name="version" >
                <FcInput />
              </Form.Item>
              <Form.Item label="场景功能描述" name="sceneDescription" >
                <FcInput />
              </Form.Item>
              <Form.Item style={{ marginRight: 8 }}
              >
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>

              <Button htmlType="button" onClick={this.onReset}>
                Reset
              </Button>
            </Space>
          </Form>

        </div>

        <FcCard
          title="任务服务"
          extra={
            <FcButton
              type="primary"
              onClick={() => {
                this.setState({ addModal: true });
              }}
            >
              {' '}
              +添加
            </FcButton>


          }
          bordered={false}
        >

          <div>
            <div style={{ marginBottom: 16 }}>
              <Space>
                <FcButton type="primary" onClick={this.exportExcel} disabled={!hasSelected} loading={loading}>
                  导出Excel
                </FcButton>
                <FcButton type="primary" onClick={this.exportPDF} disabled={!hasSelected} loading={loading}>
                  导出Pdf
                </FcButton>
              
              </Space>
              <span style={{ marginLeft: 8 }}>
                {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
              </span>
            </div>
            <FcTable rowKey="seqNo" rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.datas} />
          </div>
        </FcCard>

        <FcModal
          cancelText="取消"
          okText="确认"
          title="提示"
          visible={this.state.isModalVisible}
          onOk={this.handleOk}
          onCancel={() => {
            this.setState({ isModalVisible: false });
          }}
        >
          是否确定删除本条数据？
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="新增"
          title="用户新增"
          visible={this.state.addModal}
          onOk={this.onFinish}
          onCancel={() => {
            this.setState({
              addModal: false,
            });
          }}
        >
          <Form {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish} labelAlign={'left'}>
            <Form.Item label="接口ID" name="interfaceId" >
              <FcInput />
            </Form.Item>
            <Form.Item label="服务代码" name="svcCode" >
              <FcInput />
            </Form.Item>
            <Form.Item label="服务标识" name="svcInd" >
              <FcInput />
            </Form.Item>
            <Form.Item label="服务名称" name="svcIndName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="标准接口标识" name="stdIntfInd" >
              <FcInput />
            </Form.Item>
            <Form.Item label="标准接口名称" name="stdIntfIndName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="消费方" name="consumer" >
              <FcInput />
            </Form.Item>
            <Form.Item label="消费方服务标识" name="consumerSvcInd" >
              <FcInput />
            </Form.Item>
            <Form.Item label="消费方服务名称" name="consumerSvcIndName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="提供方" name="prodiver" >
              <FcInput />
            </Form.Item>
            <Form.Item label="提供方服务标识" name="prodiverSvcInd" >
              <FcInput />
            </Form.Item>
            <Form.Item label="提供方服务名称" name="prodiverSvcIndName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="版本号" name="version" >
              <FcInput />
            </Form.Item>
            <Form.Item label="接口状态" name="interfaceStuat" >
              <FcInput />
            </Form.Item>
            <Form.Item label="服务BSD版本" name="svcBsdVersion" >
              <FcInput />
            </Form.Item>
            <Form.Item label="场景功能描述" name="sceneDescription" >
              <FcInput />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="修改"
          title="用户编辑"
          visible={this.state.editModal}
          onOk={this.editUserOK}
          onCancel={() => {
            this.setState({ editModal: false });
          }}
        >
          <Form {...layout} ref={this.editformRef} name="control-ref" onFinish={this.onFinishEdit} labelAlign={'left'}>

            <Form.Item label="接口ID" name="interfaceId" >
              <FcInput defaultValue={this.state.editProperty.interfaceId} />
            </Form.Item>
            <Form.Item label="服务代码" name="svcCode" >
              <FcInput defaultValue={this.state.editProperty.svcCode} />
            </Form.Item>
            <Form.Item label="服务标识" name="svcInd" >
              <FcInput defaultValue={this.state.editProperty.svcInd} />
            </Form.Item>
            <Form.Item label="服务名称" name="svcIndName" >
              <FcInput defaultValue={this.state.editProperty.svcIndName} />
            </Form.Item>
            <Form.Item label="标准接口标识" name="stdIntfInd" >
              <FcInput defaultValue={this.state.editProperty.stdIntfInd} />
            </Form.Item>
            <Form.Item label="标准接口名称" name="stdIntfIndName" >
              <FcInput defaultValue={this.state.editProperty.stdIntfIndName} />
            </Form.Item>
            <Form.Item label="消费方" name="consumer" >
              <FcInput defaultValue={this.state.editProperty.consumer} />
            </Form.Item>
            <Form.Item label="消费方服务标识" name="consumerSvcInd" >
              <FcInput defaultValue={this.state.editProperty.consumerSvcInd} />
            </Form.Item>
            <Form.Item label="消费方服务名称" name="consumerSvcIndName" >
              <FcInput defaultValue={this.state.editProperty.consumerSvcIndName} />
            </Form.Item>
            <Form.Item label="提供方" name="prodiver" >
              <FcInput defaultValue={this.state.editProperty.prodiver} />
            </Form.Item>
            <Form.Item label="提供方服务标识" name="prodiverSvcInd" >
              <FcInput defaultValue={this.state.editProperty.prodiverSvcInd} />
            </Form.Item>
            <Form.Item label="提供方服务名称" name="prodiverSvcIndName" >
              <FcInput defaultValue={this.state.editProperty.prodiverSvcIndName} />
            </Form.Item>
            <Form.Item label="版本号" name="version" >
              <FcInput defaultValue={this.state.editProperty.version} />
            </Form.Item>
            <Form.Item label="接口状态" name="interfaceStuat" >
              <FcInput defaultValue={this.state.editProperty.interfaceStuat} />
            </Form.Item>
            <Form.Item label="服务BSD版本" name="svcBsdVersion" >
              <FcInput defaultValue={this.state.editProperty.svcBsdVersion} />
            </Form.Item>
            <Form.Item label="场景功能描述" name="sceneDescription" >
              <FcInput defaultValue={this.state.editProperty.sceneDescription} />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          title="提示"
          visible={this.state.isDownloadXmlVisible}
          onCancel={() => {
            this.setState({ isDownloadXmlVisible: false });
          }}
        >

          <a href={this.state.xmlLink} target="_blank">点击下载</a>

        </FcModal>
      </div>
    );
  }
}

export default DataTable;
