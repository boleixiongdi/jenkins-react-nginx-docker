// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
    sorter: {
      compare: (b: { seqNo: number; }, a: { seqNo: number; }) => a.seqNo - b.seqNo,
      multiple: 1,
    },
    defaultSortOrder: 'descend'
  },
  {
    title: '日志类型',
    dataIndex: 'journalType',
    key: 'journalType',
  },
  {
    title: '日志描述',
    dataIndex: 'description',
    key: 'description',
  },
  {
    title: '日志日期',
    dataIndex: 'creDte',
    key: 'creDte',
    sorter: (a: { creDte: string | number | Date; }, b: { creDte: string | number | Date; }) => { 
      let aTime = new Date(a.creDte).getTime();
      let bTime = new Date(b.creDte).getTime();
      return aTime - bTime;
    },

  },
  {
    title: '所属系统',
    dataIndex: 'system',
    key: 'system',
  },


];
export default columnsData;
