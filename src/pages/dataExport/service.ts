import { request } from 'ngfe-request';
// 目前我这里的接口用是模客数据


//上传
export function upload(params: object,isCover: number) {
  
  // 此处的 /function/info/list 是提供给 mock 使用的，在生产环境中无效
  return request('/api/dy/ngAtomicInterfaceQueryById', {
    // HTTP 请求体
    data: {
      // 系统报文头
      sysHead: {
        stdSvcInd: 'DataDictionarySVC',
        stdIntfcInd: 'dataDictionaryImportExcel',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      // 本地报文头
      localHead: {
        "reptChkSrlNo": "",
        "smyCdDsc": "",
        "crnPgCnt": "",
        "totLineNum1": ""
      },
      // 报文体
      body: {
        NgDataDictionaryList:params,cover:isCover
      },
    },
  });
}
// 用户查询
export function getLogList() {
  
  // 此处的 /function/info/list 是提供给 mock 使用的，在生产环境中无效
  return request('/api/dy/ngAtomicServiceSelect', {
    // HTTP 请求体
    data: {
      // 系统报文头
      sysHead: {
        stdSvcInd: 'ImportJournalSVC',
        stdIntfcInd: 'selectAll',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      // 本地报文头
      localHead: {
        "reptChkSrlNo": "",
        "smyCdDsc": "",
        "crnPgCnt": "",
        "totLineNum1": ""
      },
      // 报文体
      body: {
      },
    },
  });
}

// 用户数据删除
export async function logDelete(params: object) {
  console.log("delete:")
  console.log(params)
  return request('/api/user/userDelete', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'ImportJournalSVC',
        stdIntfcInd: 'importJournalBatchDelete',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
       logIds: params,
      },
    },
  });
}
// 用户数据删除
export async function logDeleteAll() {
  return request('/api/user/userDelete', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'ImportJournalSVC',
        stdIntfcInd: 'importJournalDeleteAll',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
      },
    },
  });
}



