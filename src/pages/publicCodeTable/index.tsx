/* 用户信息管理页 类组件编程*/
import { Space, Form, Button, Select } from 'antd';
import { FormInstance } from 'antd/es/form';
import ExportJsonExcel from 'js-export-excel';
import { router } from 'dva';
const { Link } = router;
const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: 'left'
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};


import React, { Component } from 'react';
import {
  FcTable,
  FcButton,
  FcInput,
  FcMessage,
  FcModal,
  FcDivider,
  FcCard,
} from '@ngfed/fc-components';
import {
  getConditionSearch,
  getUserList,
  userDelete,
  userListAdd,
  userListUpdate,
} from './service';
import columnsData from './data';
import styles from './index.less';

class DataTable extends Component {
  formRef = React.createRef<FormInstance>();
  editformRef = React.createRef<FormInstance>();
  searchformRef = React.createRef<FormInstance>();


  state = {
    // 根据条件查询的列表数据
    datas: [],
    // 表头
    columns: [],
    // 删除提示框显示状态
    isModalVisible: false,
    //下载提示框显示状态
    isDownloadVisible: false,
    //下载链接
    downloadLink: '',
    // 删除信息Id和修改信息Id
    id: '',
    // 查询条件姓名
    name: '',
    // 用户添加模态框显示状态
    addModal: false,
    // 用户编辑模态框显示状态
    editModal: false,
    // 当前页码
    current: 1,
    // 每页显示数量
    pageSize: 5,

    selectedRowKeys: [], // Check here to configure the default column

    loading: false,


  };

  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
      });
    }, 1000);
    let arr = new Set(this.state.datas)
    let arr2 = new Set(this.state.selectedRowKeys)
    let arr3 = Array.from(arr.filter(value => {
      return arr2.has(value.seqNo)
    }))

    this.handleExportCurrentExcel(arr3)
  };


  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  onFinish = (values: any) => {
    console.log(values);
    userListAdd(values).then(async (res: any) => {
      this.setState({ addModal: false });
      FcMessage.success(res.sysHead.retInf);
      (this as any).formRef.current.resetFields();
      this.getDatas()

    });
  };
  onFinishEdit = (values: any) => {
    console.log("sssaaadddssx" + this.state.id)
    console.log(values);
    userListUpdate(this.state.id, values).then(async (res: any) => {
      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ editModal: false });
        FcMessage.success(res.sysHead.retInf);

      }
    });
  };



  // 生命周期 数据初始化方法建议写在这个里面
  componentDidMount() {
    this.tableHeader();
    //不是错误
    this.getDatas();
  }

  // 表格头部数据重组
  tableHeader = () => {
    let columns: any = columnsData;
    const obj = {
      title: 'address',
      dataIndex: 'address',
      key: 'address',
      render: (text: any, record: any) => (
        <div>
          <a onClick={() => this.handleRemove(record.seqNo)}>删除</a>
          <FcDivider type="vertical" />
          <Link to={
            {
              pathname: `/publicCodeEnum`,
              state: { codeName: record.codeName }
            }
          }>
            维护</Link>

        </div>
      ),
    };

    columns = [...columns, obj];
    this.setState({
      columns,
    });
  };

  /**
   * 数据查询
   * @param params 查询的参数对象
   */
  getDatas = () => {
    getUserList().then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };
  //条件搜索
  getConditionSearch = (params: object) => {
    console.log(params)
    getConditionSearch(params).then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };


  /**
   * 显示模态框提示是否删除
   * @param id 用户id
   */
  handleRemove = (id: string) => {
    this.setState({
      id,
      isModalVisible: true,
    });
  };

  /**
 * 转跳维护界面
 * @param id 用户id
 */
  handleUphold = (id: string) => {
    <Link to="/dictionaryformat"></Link>

  };

  // 确定删除用户数据
  handleOk = () => {
    userDelete({ seqNo: this.state.id }).then((res: any) => {

      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ isModalVisible: false });
        FcMessage.success(res.sysHead.retInf);
      }
    });
  };






  onReset = () => {
    this.searchformRef.current!.resetFields();
    this.getDatas()
  };






  render() {

    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        {/* className 规范 */}
        {/* 单个class */}
        {/* <div  className={ styles.boxTitle }></div> */}
        {/* 多个class */}
        {/* <div  className={ `${styles.boxTitle} ${styles.boxText}` }></div> */}
        <div className={styles.box}>
          <Space size={[8, 16]} wrap>

            <Form {...layout} ref={this.searchformRef} name="control-ref" onFinish={this.getConditionSearch} labelAlign={'left'} layout="inline">
              <Form.Item label="代码名称" name="codeName" >
                <FcInput />
              </Form.Item>
              <Form.Item label="中文名称" name="chineseName" >
                <FcInput />
              </Form.Item>
              <Form.Item label="中文别名" name="chineseNameAlias" >
                <FcInput />
              </Form.Item>
              <Form.Item label="是否标准代码" name="standardCodeState" >
                <FcInput />
              </Form.Item>
              <Form.Item label="主代码数据来源" name="range" >
                <FcInput />
              </Form.Item>
              <Form.Item label="代码状态" name="codeState" >
                <FcInput />
              </Form.Item>
              <Form.Item label="修订人" name="updTlr" >
                <FcInput />
              </Form.Item>
              <Form.Item label="修订时间" name="updDte" >
                <FcInput />
              </Form.Item>

              <Form.Item style={{ marginRight: 8 }}
              >
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
              </Form.Item>

              <Button htmlType="button" onClick={this.onReset}>
                Reset
              </Button>
            </Form>
          </Space>

        </div>
      <p/>


        <div style={{ marginBottom: 16 }}>

          <FcTable rowKey="seqNo" rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.datas} />
        </div>


        <FcModal
          cancelText="取消"
          okText="确认"
          title="提示"
          visible={this.state.isModalVisible}
          onOk={this.handleOk}
          onCancel={() => {
            this.setState({ isModalVisible: false });
          }}
        >
          是否确定删除本条数据？
        </FcModal>









      </div>
    );
  }
}

export default DataTable;
