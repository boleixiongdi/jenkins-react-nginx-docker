// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '代码名称',
    dataIndex: 'codeName',
    key: 'codeName',
  },
  {
    title: '中文名称',
    dataIndex: 'chineseName',
    key: 'chineseName',
  },
  {
    title: '是否标准代码',
    dataIndex: 'standardCodeState',
    key: 'standardCodeState',
  },
  {
    title: '代码状态',
    dataIndex: 'codeState',
    key: 'codeState',
  },
  {
    title: '主代码数据来源',
    dataIndex: 'range',
    key: 'range',
  },
  {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: '修订人',
    dataIndex: 'updTlr',
    key: 'updTlr',
  },
  {
    title: '修订时间',
    dataIndex: 'updDte',
    key: 'updDte',
  }
 
];
export default columnsData;
