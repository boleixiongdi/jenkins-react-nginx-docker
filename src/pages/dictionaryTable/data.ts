// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
    sorter: {
      compare: (b: { seqNo: number; }, a: { seqNo: number; }) => a.seqNo - b.seqNo,
      multiple: 1,
    },
  },
  {
    title: '元数据名称',
    dataIndex: 'metadataId',
    key: 'metadataId',
  },
  {
    title: '中文名称',
    dataIndex: 'chineseName',
    key: 'chineseName',
  },
  {
    title: '英文全称',
    dataIndex: 'metadataName',
    key: 'metadataName',
  },
  {
    title: '类型',
    dataIndex: 'type',
    key: 'type',
  },
  {
    title: '长度',
    dataIndex: 'length',
    key: 'length',
  },
  {
    title: '数据项分类',
    dataIndex: 'categoryWordId',
    key: 'categoryWordId',
  },
  {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: '操作用户',
    dataIndex: 'updTlr',
    key: 'updTlr',
  },
  {
    title: '操作时间',
    dataIndex: 'updTime',
    key: 'updTime',
  }
 
];
export default columnsData;
