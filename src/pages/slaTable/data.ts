// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
    sorter: {
      compare: (b: { seqNo: number; }, a: { seqNo: number; }) => a.seqNo - b.seqNo,
      multiple: 1,
    },
  },
  {
    title: 'SLA指标',
    dataIndex: 'slaIndex',
    key: 'slaIndex',
  },
  {
    title: '取值范围',
    dataIndex: 'valueRange',
    key: 'valueRange',
  },
  {
    title: '描述',
    dataIndex: 'description',
    key: 'description',
  },
  {
    title: '备注',
    dataIndex: 'remark',
    key: 'remark',
  },
 
];
export default columnsData;
