/* 用户信息管理页 类组件编程*/
import { Space, Form, Button, Select, Descriptions } from 'antd';
import { FormInstance } from 'antd/es/form';
import ExportJsonExcel from 'js-export-excel';
import { router } from 'dva';
const { Link } = router;
const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: 'left'
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};


import React, { Component } from 'react';
import {
  FcTable,
  FcButton,
  FcInput,
  FcMessage,
  FcModal,
  FcDivider,
  FcCard,
} from '@ngfed/fc-components';
import {
  getUserList,
  userDelete,
  userListAdd,
  userListUpdate,
} from './service';
import columnsData from './data';
import styles from './index.less';

class DataTable extends Component {
  formRef = React.createRef<FormInstance>();
  editformRef = React.createRef<FormInstance>();
  searchformRef = React.createRef<FormInstance>();


  state = {
    //sla父类retrievalTable实体属性
    retrievalVO: [],
    // 根据条件查询的列表数据
    datas: [],
    // 表头
    columns: [],
    // 删除提示框显示状态
    isModalVisible: false,
    // 删除信息Id和修改信息Id
    id: '',
    // 查询条件姓名
    name: '',
    // 用户添加模态框显示状态
    addModal: false,
    // 用户编辑模态框显示状态
    editModal: false,
    // 当前页码
    current: 1,
    // 每页显示数量
    pageSize: 5,

    selectedRowKeys: [], // Check here to configure the default column

    loading: false,

    editProperty: []
  };




  //表单的多选
  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  //添加的接口
  onFinish = (values: any) => {
    console.log(values);
    const svcCode = this.state.retrievalVO.svcCode
    userListAdd(values, svcCode).then(async (res: any) => {
      this.setState({ addModal: false });
      FcMessage.success(res.sysHead.retInf);
      (this as any).formRef.current.resetFields();
      this.getDatas(this.state.retrievalVO.svcCode)
    });


  };
  //修改的接口
  onFinishEdit = (values: any) => {
    userListUpdate(this.state.id, values).then(async (res: any) => {
      if (res.sysHead.retCd === '000000') {
        this.getDatas(this.state.retrievalVO.svcCode);
        this.setState({ editModal: false });
        FcMessage.success(res.sysHead.retInf);

      }
    });
  };





  componentDidMount() {
    this.tableHeader();
    this.getDatas(this.state.retrievalVO.svcCode);
  }
  componentWillMount() {
    this.setState({
      retrievalVO: this.props.location.state.retrievalVO
    })
  }

  // 表格头部数据重组
  tableHeader = () => {
    let columns: any = columnsData;
    const obj = {
      title: '操作',
      dataIndex: 'address',
      key: 'address',
      render: (text: any, record: any) => (
        <div>
          <a onClick={() => this.editUser(record)}>修改</a>
          <FcDivider type="vertical" />
          <a onClick={() => this.handleRemove(record.seqNo)}>删除</a>
          <FcDivider type="vertical" />
        </div>
      ),
    };

    columns = [...columns, obj];
    this.setState({
      columns,
    });
  };

  /**
   * 数据查询
   * @param params 查询的参数对象
   */
  getDatas = (params: string) => {
    getUserList(params).then(async (res: any) => {
      console.log(res)
      this.setState({
        datas: res.body,
      });
    });
  };



  /**
   * 显示模态框提示是否删除
   * @param id 用户id
   */
  handleRemove = (id: string) => {
    this.setState({
      id,
      isModalVisible: true,
    });
  };

  // 确定删除用户数据
  handleOk = () => {
    userDelete({ seqNo: this.state.id }).then((res: any) => {

      if (res.sysHead.retCd === '000000') {
        this.getDatas(this.state.retrievalVO.svcCode);
        this.setState({ isModalVisible: false });
        FcMessage.success(res.sysHead.retInf);
      }
    });
  };



  /**
   * 编辑-点击打开模态框-回显
   * @param item 行对象数据
   */
  editUser = async (item: any) => {
    await this.setState({
      editModal: true,
      id: item.seqNo,
      editProperty: item
    });
    // setTimeout(() => {
    //   (this as any).editFormRef.current.setFieldsValue(item);
    // }, 0);
  };

  // 保存编辑值-修改
  editUserOK = () => {
    const param = (this as any).editFormRef.current.getFieldsValue();
    console.log("修改++++++" + this.state.id)
  };

  //重置查询输入框
  onReset = () => {
    this.searchformRef.current!.resetFields();
  };






  render() {
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        {/* className 规范 */}
        {/* 单个class */}
        {/* <div  className={ styles.boxTitle }></div> */}
        {/* 多个class */}
        {/* <div  className={ `${styles.boxTitle} ${styles.boxText}` }></div> */}
        <div className={styles.box}>
          <Space> <Descriptions title="服务接口 Info" bordered={true} column={5}>
            <Descriptions.Item label="接口ID">{this.state.retrievalVO.interfaceId}</Descriptions.Item>
            <Descriptions.Item label="服务代码">{this.state.retrievalVO.svcCode}</Descriptions.Item>
            <Descriptions.Item label="服务标识">{this.state.retrievalVO.svcInd}</Descriptions.Item>
            <Descriptions.Item label="服务名称">{this.state.retrievalVO.svcIndName}</Descriptions.Item>
            <Descriptions.Item label="标准接口标识">{this.state.retrievalVO.stdIntfInd}</Descriptions.Item>
            <Descriptions.Item label="标准接口名称">{this.state.retrievalVO.stdIntfIndName}</Descriptions.Item>
            <Descriptions.Item label="消费方">{this.state.retrievalVO.consumer}</Descriptions.Item>
            <Descriptions.Item label="消费方服务标识">{this.state.retrievalVO.consumerSvcInd}</Descriptions.Item>
            <Descriptions.Item label="消费方服务名称">{this.state.retrievalVO.consumerSvcIndName}</Descriptions.Item>
            <Descriptions.Item label="提供方">{this.state.retrievalVO.prodiver}</Descriptions.Item>
            <Descriptions.Item label="提供方服务标识">{this.state.retrievalVO.prodiverSvcInd}</Descriptions.Item>
            <Descriptions.Item label="提供方服务名称">{this.state.retrievalVO.prodiverSvcIndName}</Descriptions.Item>
            <Descriptions.Item label="版本号">{this.state.retrievalVO.version}</Descriptions.Item>
            <Descriptions.Item label="接口状态">{this.state.retrievalVO.interfaceStuat}</Descriptions.Item>
            <Descriptions.Item label="服务BSD版本">{this.state.retrievalVO.svcBsdVersion}</Descriptions.Item>
            <Descriptions.Item label="场景功能描述">{this.state.retrievalVO.sceneDescription}</Descriptions.Item>
          </Descriptions>
          </Space>
        </div>
        <p />

        <FcCard
          title="服务SLA"
          extra={
            <FcButton
              type="primary"
              onClick={() => {
                this.setState({ addModal: true });
              }}
            >
              {' '}
              +添加
            </FcButton>


          }
          bordered={false}
        >
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
          <div>
            <FcTable rowKey="seqNo" rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.datas} />
          </div>
        </FcCard>

        <FcModal
          cancelText="取消"
          okText="确认"
          title="提示"
          visible={this.state.isModalVisible}
          onOk={this.handleOk}
          onCancel={() => {
            this.setState({ isModalVisible: false });
          }}
        >
          是否确定删除本条数据？
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="新增"
          title="用户新增"
          visible={this.state.addModal}
          onOk={this.onFinish}
          onCancel={() => {
            this.setState({
              addModal: false,
            });
          }}
        >
          <Form {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish} labelAlign={'left'}>
            <Form.Item label="SLA指标" name="slaIndex" >
              <FcInput />
            </Form.Item>
            <Form.Item label="取值范围" name="valueRange" >
              <FcInput />
            </Form.Item>
            <Form.Item label="描述" name="description" >
              <FcInput />
            </Form.Item>
            <Form.Item label="备注" name="remark" >
              <FcInput />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="修改"
          title="用户编辑"
          visible={this.state.editModal}
          onOk={this.editUserOK}
          onCancel={() => {
            this.setState({ editModal: false });
          }}
        >
          <Form {...layout} ref={this.editformRef} name="control-ref" onFinish={this.onFinishEdit} labelAlign={'left'}>

            <Form.Item label="SLA指标" name="slaIndex">
              <FcInput defaultValue={this.state.editProperty.slaIndex} />
            </Form.Item>
            <Form.Item label="取值范围" name="valueRange" >
              <FcInput defaultValue={this.state.editProperty.valueRange} />
            </Form.Item>
            <Form.Item label="描述" name="description">
              <FcInput defaultValue={this.state.editProperty.description} />
            </Form.Item>
            <Form.Item label="备注" name="remark" >
              <FcInput defaultValue={this.state.editProperty.remark} />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>


      </div>
    );
  }
}

export default DataTable;
