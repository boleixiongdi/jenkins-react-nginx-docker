import { request } from 'ngfe-request';
// 目前我这里的接口用是模客数据

// 通过主键查询单条数据
export async function userQuery(params: object) {
  console.log("query:")
  console.log(params)
  return request('/api/dy/ngAtomicSlaserviceQueryById', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'CallerProviderSVC',
        stdIntfcInd: 'ngAtomicSlaserviceQueryById',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,
      },
    },
  });
}

// 用户查询
export async function getUserList(params: string) {
  return request('/api/dy/ngAtomicSlaserviceSelect', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'CallerProviderSVC',
        stdIntfcInd: 'queryBySvcCode',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
      svcCode :params,
      },
    },
  });
}

// 用户数据删除
export async function userDelete(params: object) {
  console.log("delete:")
  console.log(params)
  return request('/api/dy/ngAtomicSlaserviceDeleteById', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'CallerProviderSVC',
        stdIntfcInd: 'deleteById',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,
      },
    },
  });
}

// 用户数据添加
export async function userListAdd(params: object,svcCode) {
  return request('/api/dy/ngAtomicSlaserviceInsert', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'CallerProviderSVC',
        stdIntfcInd: 'insert',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,svcCode:svcCode
      },
    },
  });
}

// 用户数据修改
export async function userListUpdate(id: string,params: object) {
  return request('/api/user/retrievalUpdate', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: 'CallerProviderSVC',
        stdIntfcInd: 'update',
        stdIntfcVerNo: '1.0.0',
        srcConsmSysInd: 'NGOCTS',
      },
      body: {
        ...params,seqNo:id
      },
    },
  });
}
