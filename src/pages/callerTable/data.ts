// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '系统名称',
    dataIndex: 'callSystemName',
    key: 'callSystemName',
  },
  {
    title: '接口名称',
    dataIndex: 'callInterfaceName',
    key: 'callInterfaceName',
  },
  {
    title: '接口代码',
    dataIndex: 'callInterfaceCode',
    key: 'callInterfaceCode',
  },
  {
    title: '交易码',
    dataIndex: 'callTransactionCode',
    key: 'callTransactionCode',
  },
  {
    title: '是否标准',
    dataIndex: 'callStandard',
    key: 'callStandard',
  },
  {
    title: '系统名称',
    dataIndex: 'provideSystemName',
    key: 'provideSystemName',
  },
  {
    title: '接口名称',
    dataIndex: 'provideInterfaceName',
    key: 'provideInterfaceName',
  },
  {
    title: '接口代码',
    dataIndex: 'provideInterfaceCode',
    key: 'provideInterfaceCode',
  },
  {
    title: '交易码',
    dataIndex: 'provideTransactionCode',
    key: 'provideTransactionCode',
  },
  {
    title: '是否标准',
    dataIndex: 'provideStandard',
    key: 'provideStandard',
  },
  {
    title: '开发环境',
    dataIndex: 'developEnviorment',
    key: 'developEnviorment',
  },
  {
    title: 'SIT环境',
    dataIndex: 'sitEnviorment',
    key: 'sitEnviorment',
  },
  {
    title: 'UAT环境',
    dataIndex: 'uatEnviorment',
    key: 'uatEnviorment',
  },
];
export default columnsData;
