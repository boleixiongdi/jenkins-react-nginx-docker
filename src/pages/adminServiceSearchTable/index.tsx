/* 用户信息管理页 类组件编程*/
import { Space, Form, Button, Select } from 'antd';
import { FormInstance } from 'antd/es/form';
import ExportJsonExcel from 'js-export-excel';

const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: 'left'
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};


import React, { Component } from 'react';
import {
  FcTable,
  FcButton,
  FcInput,
  FcMessage,
  FcModal,
  FcDivider,
  FcCard,
} from '@ngfed/fc-components';
import {
  getConditionSearch,
  getUserList,
  userDelete,
  userListAdd,
  userListUpdate,
} from './service';
import columnsData from './data';
import styles from './index.less';

class DataTable extends Component {
  formRef = React.createRef<FormInstance>();
  editformRef = React.createRef<FormInstance>();
  searchformRef = React.createRef<FormInstance>();


  state = {
    // 根据条件查询的列表数据
    datas: [],
    // 表头
    columns: [],
    // 删除提示框显示状态
    isModalVisible: false,
    //下载提示框显示状态
    isDownloadVisible: false,
    //下载链接
    downloadLink: '',
    // 删除信息Id和修改信息Id
    id: '',
    // 查询条件姓名
    name: '',
    // 用户添加模态框显示状态
    addModal: false,
    // 用户编辑模态框显示状态
    editModal: false,
    // 当前页码
    current: 1,
    // 每页显示数量
    pageSize: 5,

    selectedRowKeys: [], // Check here to configure the default column

    loading: false,


  };



  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  onFinish = (values: any) => {
    console.log(values);
    userListAdd(values).then(async (res: any) => {
      this.setState({ addModal: false });
      FcMessage.success(res.sysHead.retInf);
      (this as any).formRef.current.resetFields();
      this.getDatas()

    });
  };
  onFinishEdit = (values: any) => {
    console.log("sssaaadddssx" + this.state.id)
    console.log(values);
    userListUpdate(this.state.id, values).then(async (res: any) => {
      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ editModal: false });
        FcMessage.success(res.sysHead.retInf);

      }
    });
  };



  // 生命周期 数据初始化方法建议写在这个里面
  componentDidMount() {
    this.tableHeader();
    //
    this.getDatas();
  }

  // 表格头部数据重组
  tableHeader = () => {
    let columns: any = columnsData;
 

    columns = [...columns];
    this.setState({
      columns,
    });
  };

  /**
   * 数据查询
   * @param params 查询的参数对象
   */
  getDatas = () => {
    getUserList().then(async (res: any) => {
      console.log('res', res?.sysHead?.retCd);
      this.setState({
        datas: res.body,
      });
    });
  };
  //条件搜索
  getConditionSearch = (params: object) => {
    console.log("params")
    console.log(params)

    getConditionSearch(params).then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };
  

 
  onReset = () => {
    this.searchformRef.current!.resetFields();
    this.getDatas()
  };

 




  render() {

    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        {/* className 规范 */}
        {/* 单个class */}
        {/* <div  className={ styles.boxTitle }></div> */}
        {/* 多个class */}
        {/* <div  className={ `${styles.boxTitle} ${styles.boxText}` }></div> */}
        <div className={styles.box}>
          <Form {...layout} ref={this.searchformRef} name="control-ref" onFinish={this.getConditionSearch} labelAlign={'left'} layout="inline">
            <Form.Item label="接口ID" name="interfaceId" >
            <FcInput />
            </Form.Item>
            <Form.Item label="交易码" name="transactionCode" >
              <FcInput />
            </Form.Item>
            <Form.Item label="交易名称" name="transactionName" >
              <FcInput />
            </Form.Item>

            <Form.Item style={{ marginRight: 8 }}
            >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </Form.Item>

            <Button htmlType="button" onClick={this.onReset}>
              Reset
            </Button>
          </Form>

        </div>

    

            <div style={{ marginBottom: 16 }}>
           
            <FcTable rowKey="seqNo" rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.datas} />
          </div>
        

        
      </div>
    );
  }
}

export default DataTable;
