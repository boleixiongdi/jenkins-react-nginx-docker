// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '接口ID',
    dataIndex: 'interfaceId',
    key: 'interfaceId',
  },
  {
    title: '交易码',
    dataIndex: 'transactionCode',
    key: 'transactionCode',
  },
  {
    title: '交易名称',
    dataIndex: 'transactionName',
    key: 'transactionName',
  },
  {
    title: '提供方服务标识',
    dataIndex: 'providerSvcInd',
    key: 'providerSvcInd',
  },
  {
    title: '提供方服务名',
    dataIndex: 'providerSerciveName',
    key: 'providerSerciveName',
  },
  {
    title: '提供方系统ID',
    dataIndex: 'providerSystemId',
    key: 'providerSystemId',
  },
  {
    title: '是否开启穿透',
    dataIndex: 'natFlag',
    key: 'natFlag',
  },
  {
    title: '接口包名全路径',
    dataIndex: 'interfaceFullPath',
    key: 'interfaceFullPath',
  }
 
];
export default columnsData;
