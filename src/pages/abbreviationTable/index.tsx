/* 用户信息管理页 类组件编程*/
import { Form, Button, Select } from 'antd';
import { FormInstance } from 'antd/es/form';

const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: 'left'
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};


import React, { Component } from 'react';
import {
  FcTable,
  FcButton,
  FcInput,
  FcMessage,
  FcModal,
  FcDivider,
  FcCard,
} from '@ngfed/fc-components';
import {
  getUserList,
  userDelete,
  userListAdd,
  userListUpdate,
  getConditionSearch
} from './service';
import columnsData from './data';
import styles from './index.less';

class DataTable extends Component {
  formRef = React.createRef<FormInstance>();
  editformRef = React.createRef<FormInstance>();
  searchformRef = React.createRef<FormInstance>();


  state = {
    // 根据条件查询的列表数据
    datas: [],
    // 表头
    columns: [],
    // 删除提示框显示状态
    isModalVisible: false,
    // 删除信息Id和修改信息Id
    id: '',
    // 查询条件姓名
    name: '',
    // 用户添加模态框显示状态
    addModal: false,
    // 用户编辑模态框显示状态
    editModal: false,
    // 当前页码
    current: 1,
    // 每页显示数量
    pageSize: 5,

    selectedRowKeys: [], // Check here to configure the default column

    loading: false,
  };




  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  onFinish = (values: any) => {
    userListAdd(values).then(async (res: any) => {
      this.setState({ addModal: false });
      FcMessage.success(res.sysHead.retInf);
      (this as any).formRef.current.resetFields();
      this.getDatas()

    });
  };
  onFinishEdit = (values: any) => {
    userListUpdate(this.state.id, values).then(async (res: any) => {
      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ editModal: false });
        FcMessage.success(res.sysHead.retInf);

      }
    });
  };





  // 生命周期 数据初始化方法建议写在这个里面
  componentDidMount() {
    this.tableHeader();
    this.getDatas();
  }

  // 表格头部数据重组
  tableHeader = () => {
    let columns: any = columnsData;
    const obj = {
      title: '操作',
      dataIndex: 'address',
      key: 'address',
      render: (text: any, record: any) => (
        <div>
          <a onClick={() => this.editUser(record)}>修改</a>
          <FcDivider type="vertical" />
          <a onClick={() => this.handleRemove(record.seqNo)}>删除</a>
        </div>
      ),
    };

    columns = [...columns, obj];
    this.setState({
      columns,
    });
  };
  
  /**
   * 数据查询
   * @param params 查询的参数对象
   */
  getDatas = () => {
    getUserList().then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
   
    });
  };
  /**
   * 数据条件查询
   * @param params 查询的参数对象
   */
   ConditionSearch = (params: object) => {
    getConditionSearch(params).then(async (res: any) => {
      this.setState({
        datas: res.body,
      });
    });
  };


  /**
   * 显示模态框提示是否删除
   * @param id 用户id
   */
  handleRemove = (id: string) => {
    this.setState({
      id,
      isModalVisible: true,
    });
  };

  // 确定删除用户数据
  handleOk = () => {
    userDelete({ seqNo: this.state.id }).then((res: any) => {

      if (res.sysHead.retCd === '000000') {
        this.getDatas();
        this.setState({ isModalVisible: false });
        FcMessage.success(res.sysHead.retInf);
      }
    });
  };



  /**
   * 编辑-点击打开模态框-回显
   * @param item 行对象数据
   */
  editUser = async (item: any) => {
    await this.setState({
      editModal: true,
      id: item.seqNo
    });
    // setTimeout(() => {
    //   (this as any).editFormRef.current.setFieldsValue(item);
    // }, 0);
  };

  // 保存编辑值-修改
  editUserOK = () => {
    const param = (this as any).editFormRef.current.getFieldsValue();
    console.log("修改++++++" + this.state.id)
  };



  onReset = () => {
    this.searchformRef.current!.resetFields();
    this.getDatas()
  };

 




  render() {
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        {/* className 规范 */}
        {/* 单个class */}
        {/* <div  className={ styles.boxTitle }></div> */}
        {/* 多个class */}
        {/* <div  className={ `${styles.boxTitle} ${styles.boxText}` }></div> */}
        <div className={styles.box}>
          <Form {...layout} ref={this.searchformRef} name="control-ref" onFinish={this.ConditionSearch} labelAlign={'left'} layout="inline">
            <Form.Item label="词汇中文名称" name="wordChinese" >
              <FcInput style={{ width: 200 }}
              />
            </Form.Item>
            <Form.Item label="词汇英文名称" name="wordName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="词汇英文缩写" name="wordAbbreviation" >
              <FcInput />
            </Form.Item>
           
            <Form.Item style={{ marginRight: 8 }}
            >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </Form.Item>

            <Button htmlType="button" onClick={this.onReset}>
              Reset
            </Button>
          </Form>

        </div>

        <FcCard
          title="用户信息"
          extra={
            <FcButton
              type="primary"
              onClick={() => {
                this.setState({ addModal: true });
              }}
            >
              {' '}
              +添加
            </FcButton>


          }
          bordered={false}
        >

          <div>
            <div style={{ marginBottom: 16 }}>
        
              
            </div>
            <FcTable rowKey="seqNo" rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.datas} />
          </div>
        </FcCard>

        <FcModal
          cancelText="取消"
          okText="确认"
          title="提示"
          visible={this.state.isModalVisible}
          onOk={this.handleOk}
          onCancel={() => {
            this.setState({ isModalVisible: false });
          }}
        >
          是否确定删除本条数据？
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="新增"
          title="用户新增"
          visible={this.state.addModal}
          onOk={this.onFinish}
          onCancel={() => {
            this.setState({
              addModal: false,
            });
          }}
        >
          <Form {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish} labelAlign={'left'}>
            <Form.Item label="英文单词" name="wordName" rules={[{ required: true }]}>
              <FcInput />
            </Form.Item>
            <Form.Item label="单词缩写" name="wordAbbreviation" rules={[{ required: true }]}>
              <FcInput />
            </Form.Item>
            <Form.Item label="单词中文" name="wordChinese" rules={[{ required: true }]}>
              <FcInput />
            </Form.Item>
            <Form.Item label="单词备注" name="wordRemark" rules={[{ required: true }]}>
              <FcInput />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>

        <FcModal
          footer={null}
          destroyOnClose
          cancelText="取消"
          okText="修改"
          title="用户编辑"
          visible={this.state.editModal}
          onOk={this.editUserOK}
          onCancel={() => {
            this.setState({ editModal: false });
          }}
        >
          <Form {...layout} ref={this.editformRef} name="control-ref" onFinish={this.onFinishEdit} labelAlign={'left'}>

            <Form.Item label="单词名称" name="wordName" >
              <FcInput />
            </Form.Item>
            <Form.Item label="单词缩写" name="wordAbbreviation" >
              <FcInput />
            </Form.Item>
            <Form.Item label="单词缩写(小驼峰)" name="wordAbbreviationHump" >
              <FcInput />
            </Form.Item>
            <Form.Item label="单词中文" name="wordChinese">
              <FcInput />
            </Form.Item>
            <Form.Item label="单词备注" name="wordRemark" >
              <FcInput />
            </Form.Item>
  
           
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>

            </Form.Item>
          </Form>
        </FcModal>
      </div>
    );
  }
}

export default DataTable;
