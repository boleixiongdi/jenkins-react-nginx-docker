// 定义表头 涉及到需要使用 render 的 放在tsx页面上添加
let columnsData = [
  {
    title: '顺序号',
    dataIndex: 'seqNo',
    key: 'seqNo',
  },
  {
    title: '单词名称',
    dataIndex: 'wordName',
    key: 'wordName',
  },
  {
    title: '单词缩写',
    dataIndex: 'wordAbbreviation',
    key: 'wordAbbreviation',
  },
  {
    title: '单词缩写(小驼峰)',
    dataIndex: 'wordAbbreviationHump',
    key: 'wordAbbreviationHump',
  },
  {
    title: '单词中文',
    dataIndex: 'wordChinese',
    key: 'wordChinese',
  },
  {
    title: '单词备注',
    dataIndex: 'wordRemark',
    key: 'wordRemark',
  },
  {
    title: '操作用户',
    dataIndex: 'updTlr',
    key: 'updTlr',
  },
  {
    title: '操作日期',
    dataIndex: 'updTime',
    key: 'updTime',
  }

 
];
export default columnsData;
