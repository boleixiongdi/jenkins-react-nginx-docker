export default {
  // 命名空间名字，必填
  namespace: 'common',
  state: {},
  reducers: {
    handleCollapseChange(state: any, { payload }: any) {
      return { ...state, ...payload };
    },
  },
  effects: {},
};
