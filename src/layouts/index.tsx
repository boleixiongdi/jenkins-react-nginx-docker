import React, { Fragment } from 'react';
import { history } from 'umi';
import { ConfigProvider } from 'antd';
import zhCN from 'antd/es/locale/zh_CN';

class BasicLayout extends React.Component {
  changePath = ({}) => {
    //路由跳转
    history.push('/home');
  };

  render() {
    return (
      <Fragment>
        {/* ConfigProvider 是为了解决antd 默认英文字样 */}
        <ConfigProvider locale={zhCN}>
          <div>{this.props.children}</div>
        </ConfigProvider>
      </Fragment>
    );
  }
}

export default BasicLayout;
