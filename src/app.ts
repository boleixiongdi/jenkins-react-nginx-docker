import { history, withRouter } from 'umi';
import actions from '@/shared/actions';
// 全局引用naged
import '@ngfed/fc-components/dist/ngfed.min.css';

// @ts-ignore
if (process.env.NODE_ENV === 'production' && !window.__POWERED_BY_QIANKUN__) {
  history.push('/noAuth');
}

function mainAppLoadingStateChange(callback: Function, props: any, state: any) {
  console.log('I am A');
  callback(props, state); //调用该函数
}

// 通过回调的方式改变状态
function mainAppLoadingStateChangeCb(props: any, state: any) {
  console.log('I am B');
  setTimeout(() => {
    props.onGlobalStateChange((state: any, prev: any) => {
      // state: 变更后的状态; prev 变更前的状态
      console.log(state, prev);
    });
    props.setGlobalState(state);
  }, 1000);
}

export const qiankun = {
  // 初始化子应用
  async bootstrap(props: any) {
    // 子应用单独运行时 props 为空
    console.log('app1 bootstrap', props);
  },
  // 应用每次进入都会调用，通常我们在这里触发应用 render
  async mount(props: any) {
    console.log('app1 mount', props);
    console.log('app1 mount actions:', actions);
    // 注入 actions 实例
    actions.setActions(props);
    let state = {
      microAppLoading: false,
    };
    // 通过修改全局状态修改子应用加载动画
    mainAppLoadingStateChange(mainAppLoadingStateChangeCb, props, state);
  },
  // 应用卸载之后触发
  async unmount(props: any) {
    console.log('app1 unmount', props);
  },
};

// 请求服务端根据响应动态更新路由
export function patchRoutes(routes: any) {
  console.log('patchRoutes');
  console.log(routes);
}

// 渲染应用之前做权限校验，不通过则跳转到登录页
export function render(oldRender: any) {
  console.log('render');
  setTimeout(oldRender, 1000);
}

// 用于在初始加载和路由切换时做一些事情
export function onRouteChange({ location, routes, action }: any) {
  console.log('onRouteChange');
  console.log(location.pathname);
}
