const routes = [
  {
    // path不允许为空
    path: '/*',
    // 严格路由匹配模式
    exact: true,
    component: '../layouts/index',
    routes: [
      {
        name: '后管服务查询 类组件编写',
        path: '/',
        component: '../pages/adminServiceSearchTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
     
      {
        name: '导入数据页 类组件编写',
        path: '/export',
        component: '../pages/dataExport/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '类别词页 类组件编写',
        path: '/classifier',
        component: '../pages/classifierTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '英文单词及缩写管理 类组件编写',
        path: '/abbreviation',
        component: '../pages/abbreviationTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '元数据管理 类组件编写',
        path: '/dictionary',
        component: '../pages/dictionaryTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '元数据格式管理 类组件编写',
        path: '/dictionaryformat',
        component: '../pages/dictionaryformatTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '公共代码管理 类组件编写',
        path: '/publicCode',
        component: '../pages/publicCodeTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '公共代码枚举管理 类组件编写',
        path: '/publicCodeEnum',
        component: '../pages/publicCodeEnumTable/index',
        // wrappers: ['@/routes/PrivateRoute.js'],  //接入到基座必须加上这个 本地调试可放开
      },
    
      {
        name: 'SLA服务 类组件编写',
        path: '/sla',
        component: '../pages/slaTable/index',
        //  wrappers: ['@/routes/PrivateRoute.js'],//接入到基座必须加上这个 本地调试可放开
      },
      {
        name: '调用者和接受者 类组件编写',
        path: '/call',
        component: '../pages/callerTable/index',
        //  wrappers: ['@/routes/PrivateRoute.js'],//接入到基座必须加上这个 本地调试可放开
      },
       {
        name: '服务检索 类组件编写',
        path: '/retrieval',
        component: '../pages/retrievalTable/index',
        //  wrappers: ['@/routes/PrivateRoute.js'],//接入到基座必须加上这个 本地调试可放开
      },
    
      {
        name: '基座中访问子应用 提示页',
        path: '/noAuth',
        component: '../pages/NoAuth/index',
        // wrappers: ['@/routes/PrivateRoute.js'],
      },
      { name: '404', path: '/*', component: '../pages/404', exact: false },
    ],
  },
];
export default routes;
